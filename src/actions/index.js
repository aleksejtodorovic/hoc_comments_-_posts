import axios from "axios";
import { SAVE_COMMENT, FETCH_COMMENTS, CHANGE_AUTH } from "actions/types";

export const saveComment = comment => {
  return {
    type: SAVE_COMMENT,
    payload: comment
  };
};

export const fetchComments = () => async dispatch => {
  const response = await axios.get(
    "http://jsonplaceholder.typicode.com/comments"
  );

  dispatch({
    type: FETCH_COMMENTS,
    payload: response.data
  });
};

export const changeAuth = isLogedIn => {
  return {
    type: CHANGE_AUTH,
    payload: isLogedIn
  };
};
