import React from "react";
import { connect } from "react-redux";
import { saveComment, fetchComments } from "actions";

class CommentBox extends React.Component {
  state = { comment: "" };

  onSubmit = evt => {
    evt.preventDefault();
    this.props.saveComment(this.state.comment);
    this.setState({ comment: "" });
  };

  render() {
    return (
      <div className="ui segment" onSubmit={this.onSubmit}>
        <form className="ui form">
          <div className="field">
            <h4>Add a comment</h4>
            <textarea
              value={this.state.comment}
              onChange={({ target }) =>
                this.setState({ comment: target.value })
              }
            />
          </div>
          <button className="ui button" type="submit">
            Submit
          </button>
        </form>
        <button
          id="fetch-comments"
          className="ui button"
          style={{ marginTop: "10px" }}
          onClick={this.props.fetchComments}
        >
          Fetch Comments
        </button>
      </div>
    );
  }
}

export default connect(
  null,
  { saveComment, fetchComments }
)(CommentBox);
